#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#include <pthread.h> 

#define perror2(s, e) fprintf(stderr, "%s: %s\n", s, strerror(e))

int counter=0;

static pthread_mutex_t mymutex=PTHREAD_MUTEX_INITIALIZER;

typedef struct argument *ptr;

typedef struct argument{
	int number;
}argument;

void *routine(void *arg) {
	int err; 
	ptr a=arg;
	//trylock LOCKS THE MUTEX IF IT CAN OTHERWISE IT JUST CONTINUES
	if(err=pthread_mutex_trylock(&mymutex)!=0) perror2("trylock", err);
	//if(err=pthread_mutex_lock(&mymutex)!=0) { perror2("fatal error locking", err); exit;}
	counter++;
	//if(err=pthread_mutex_unlock(&mymutex)!=0) { perror2("fatal error unlocking", err); exit;}
   fprintf(stderr,"i am here with value %d\n",a->number);
   fprintf(stderr,"thread %ld\n", pthread_self());
   fprintf(stderr,"number of total threads yet = %d\n",counter);
   if(err=pthread_mutex_unlock(&mymutex)!=0) { perror2("fatal error unlocking", err); exit;}
   pthread_exit(NULL); 
}



main(int argc, char *argv[]){ 
   int n, i, err;
   pthread_t *thread_ids;
   ptr a;
   
   if (argc > 1) n = atoi(argv[1]); /* Make integer */
   else exit(0);
   if (n > 50) { /* Avoid too many threads */
      fprintf(stderr,"Number of threads should be up to 50\n"); 
      exit(0); 
      }

   if ((thread_ids = malloc(n * sizeof(pthread_t))) == NULL) {
      perror("malloc"); 
      exit(1); 
      }


   for (i=0 ; i<n ; i++) {
   	a=malloc(sizeof(argument));
   	a->number=10;
   	if (err = pthread_create(&thread_ids[i], NULL, routine, a)) {
      	/* Create a thread */
      	perror2("pthread_create", err); 
      	exit(1); 
      	} 
   }

   for (i=0 ; i<n ; i++)
   if (err = pthread_join(thread_ids[i], NULL)) {
      /* Wait for thread termination */
      perror2("pthread_join", err); 
      exit(1); 
      }
   fprintf(stderr,"all %d threads have terminated\n", n); 
   pthread_mutex_destroy(&mymutex);
}



